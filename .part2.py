#!/usr/bin/env python

import subprocess, sys, time
from os import path

def main():


    efi = path.isdir('/sys/firmware/efi/efivars')
    boot_part = sys.argv[1] + '1'
    if efi == True:
        subprocess.call(['clear'])
        print('\nEnter a root password\n')
        subprocess.call(['passwd'])
        subprocess.call(['pacman', '-S', 'grub', 'efibootmgr', 'linux'])
        subprocess.call(['mount', boot_part, '/boot'])
        subprocess.call(['grub-install', '--target=x86_64-efi', '--efi-directory=boot', '--bootloader-id=GRUB'])
        subprocess.call(['grub-mkconfig', '-o', '/boot/grub/grub.cfg'])
        subprocess.call(['pacman', '-S', 'linux', '--noconfirm'])
        subprocess.call(['grub-mkconfig', '-o', '/boot/grub/grub.cfg'])
        subprocess.call(['ln', '-sf', '/usr/share/zoneinfo/America/New_York', '/etc/localtime'])
        subprocess.call(['sed', '-i', 's/# en_US.UTF-8 UTF-8/en_US.UTF-8/', '/etc/locale.gen'])
        subprocess.call(['locale-gen'])
        print('\n\nGood to go... type exit then press enter to return to live usb/cd')
        time.sleep(2)
        exit()

    else:
        subprocess.call(['clear'])
        print('\nEnter a root password\n')
        subprocess.call(['passwd'])
        subprocess.call(['pacman', '-S', 'grub'])
        subprocess.call(['grub-install', sys.argv[1]])
        subprocess.call(['grub-mkconfig', '-o', '/boot/grub/grub.cfg'])
        subprocess.call(['ln', '-sf', '/usr/share/zoneinfo/America/New_York', '/etc/localtime'])
        print('\n\nGood to go... type exit then press enter to return to live usb/cd')
        time.sleep(2)
        exit()



if __name__ == '__main__':
    main()
