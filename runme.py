#!/usr/bin/env python

import sys, os, time
from os import path 
import subprocess



def efi_check():
    global _check_efi
    efi = path.isdir("/sys/firmware/efi/efivars")
    
    if efi == True:
        print("\nYou have an EFI setup!")
        _check_efi = 1
    else:
        print("\nYou don't have an EFI setup!")
        _check_efi = 0


def internet_check():
    print("\nChecking if you have internet...\n")

    try:
        p = subprocess.check_call(['ping','-c3','archlinux.org'], stdout=subprocess.PIPE,
	stderr=subprocess.STDOUT)
        internet = True
    except subprocess.CalledProcessError:
        internet = False
        print("Not Connected")
	
    return internet

def clock():
    subprocess.call(['timedatectl', 'set-ntp', 'true'])


def check_disk():
    global disk_name
    subprocess.call(['clear'])
    print('\nShow all disk that can be formatted. \n')

    subprocess.call(['fdisk', '-l'])
    disk_format = input('\nWhich disk do you want to format? e.g.(/dev/sda): ')
    print('Is','\33[33m',disk_format,'\33[0m','the correct disk? y/n:', end= " ") 
    correct_disk = input()
    
    if correct_disk == 'n' or correct_disk == 'N':
        check_disk()
    else:
        print('\nI suggest you make 4 partitions, 1 \33[32mboot\33[0m, 1 \033[32mroot\033[0m 1 \033[32mhome\033[0m 1 \033[32mswap\033[0m partiton')
        time.sleep(2)
        disk_name = disk_format
        subprocess.call(['cfdisk', disk_format])



def make_filesystem(part_type, efi):
    global partition_type
    global root_part
    is_efi = efi;

    if is_efi == 0:
        if part_type == 'boot':
            partition_name = 'boot'
            partition_type = 'mkfs.ext2'
        elif part_type == 'root':
            partition_name = 'root'
            partition_type = 'mkfs.ext4'
        elif part_type == 'home':
            partition_name = 'home'
            partition_type = 'mkfs.ext4'
        elif part_type == 'swap':
            partition_name = 'mkswap'
        else:
            print("Something isn't right")
            exit()
	
    else:
        if part_type == 'boot':
            partition_name = 'boot'
            partition_type = 'mkfs.vfat'
        elif part_type == 'root':
            partition_name = 'root'
            partition_type = 'mkfs.ext4'
        elif part_type == 'home':
            partition_name = 'home'
            partition_type = 'mkfs.ext4'
        elif part_type == 'swap':
            partition_name = 'mkswap'
        else:
            print("Something isn't right")
            exit()
	


    print('\nWhich partition is your','\33[32m',part_type,'\033[0m','partition going to be on? e.g. (/dev/sda1) or press l/L to list the partitions:', end=" ")
    partition = input()

    while partition == 'l' or partition == 'L':
        subprocess.call(['clear'])
        subprocess.call(['fdisk', disk_name, '-l'])
        input('\n\nPress enter when done... ')
    
        print('\nWhich partition is your','\33[32m',part_type,'\033[0m','partition going to be on? e.g. (/dev/sda1) or press l/L to list the partitions:', end=" ")
        partition = input()

        if not partition == 'l' or partition == 'L':
            break


    print('Is','\33[32m',partition,'\33[0m','correct? y/n:', end=" ")
    check_partition = input()
  

    while check_partition == 'n' or check_partition == 'N':
        partition = check_partition = None
        print('\nWhich partition is your','\33[32m',part_type,'\033[0m','partition going to be on? e.g. (/dev/sda1):', end=" ")
        partition = input()

        print('Is','\33[32m',partition,'\33[0m','correct? y/n:', end=" ")
        check_partition = input()
   
        if check_partition == 'y' or check_partition == 'Y':
            break

    

    if part_type == 'swap':
        subprocess.call([partition_type, partition])
        subprocess.call(['swapon', partition])
    elif part_type == 'root':
        root_part = partition
        subprocess.call([partition_type, partition])
    else:
        subprocess.call([partition_type, partition])



def mount_install():
    rp = root_part
    dk = disk_name
    subprocess.call(['mount', rp, '/mnt'])
    subprocess.call(['pacstrap', '/mnt', 'base', 'base-devel', 'linux', 'linux-firmware', 'bash', 'sudo', 'vim'])
    gen = subprocess.check_output(['blkid | grep ' + rp + ' | awk ' +  " '{print $2}'"], shell=True)
    gen_to_str = str(gen)
    gen_uuid = gen_to_str.replace('"', '').replace('\\n', '').replace('b', '').replace("'", '')
    gen_directory_stuff = '\t/\text4\trw,relatime\t0 1'
    gen_final = gen_uuid + gen_directory_stuff
    with open('/mnt/etc/fstab', 'a') as fstab_file:
        fstab_file.write(gen_final)
        fstab_file.close()
# NEED TO ADD Region and City later
    subprocess.call(['hwclock', '--systohc'])
    print('What do you want your hostname to be?:', end=" ")
    hostname = input()
    with open('/mnt/etc/hostname', 'a') as h:
        h.write(hostname)
    with open('/mnt/etc/hosts', 'a') as hosts:
        host_info = '127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t'+hostname+'.localdomain\t'+hostname
        print(host_info)
        time.sleep(5)
        hosts.write(host_info)
        hosts.close()
    subprocess.call(['cp', '.part2.py', '/mnt/part2.py'])
    subprocess.call(['chmod', '+x', '/mnt/part2.py'])
    print('\n------------------- 1.) Run pacman -S python -----------------\n')
    print('------------------- 2.) Then run ./part2.py ' + dk + ' -------------------\n') 
    subprocess.call(['arch-chroot', '/mnt'])
    subprocess.call(['rm', '/mnt/part2.py'])
    subprocess.call(['clear'])
    print('\nThank you for using my program. Please reboot to launch your new arch install!')
    time.sleep(5)
    subprocess.call(['umount', '-R', '/mnt'])
    exit()



def main():
    subprocess.call(['clear'])
    if os.geteuid() != 0:
        print('\n Please re-run as root\n------------------------')
        exit()
 
    print('\nDid you read the README? y/n: ', end=" ")
    readme = input()
    if readme == 'n' or readme == 'N':
        print('\nWell here you go, you need to...')
        time.sleep(2)
        subprocess.call(['clear'])
        subprocess.call(['cat', 'README'])
        print('Press enter to continue...', end=" ")
        x = input()
        efi_check()
        is_efi = _check_efi
    else:
        efi_check()
        is_efi = _check_efi

   
    if internet_check() == False:
        print("\nYou're not connected to the internet")
        print("\nMake sure your connect before you can continue!")
        exit()
    else:
        print("\nYou're connected!")
        print("\nUpdating system clock")
        time.sleep(2)
        clock()
    
    check_disk()
    boot = 'boot'
    home = 'home'
    root = 'root'
    swap = 'swap'
    test = 'test'
    make_filesystem(boot, is_efi)
    make_filesystem(home, is_efi)
    make_filesystem(root, is_efi)
    make_filesystem(swap, is_efi)
    mount_install()


if __name__ == "__main__":
    main()




